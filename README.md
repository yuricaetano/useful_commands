# Create app por um repositorio do github 
npx-create-app -e github_repo

# Database
sudo -i -u postgres

psql

create database mydb;

create user myuser with encrypted password 'mypass';

grant all privileges on database mydb to myuser;

- Dump with postgress sql

pg_dump -c --if-exists --exclude-table=strapi_administrator -h 127.0.0.1 -U strapi -d strapi -W > file.sql

- clear if exists to avoid problems a ready exists: -c --if-exists
- dont delete table: --exclude-table=strapi_administrator
- user: -U
- port defaul: 5432
- to define port: -p port
- request password: -W 
- flow of informations: "db < file.sql" or "db > file.sql" 
 

import dump postgress sql (with database runing) = $psql -h 127.0.0.1 -U user -d database -W < file.sql

# Instalar Python com asdf Python
sudo apt-get update

sudo apt-get install --no-install-recommends make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.10.2


echo ". $HOME/.asdf/asdf.sh" >> ~/.bashrc


source ~/.bashrc


asdf plugin-add python

asdf install python 3.9.0

asdf global python 3.9.0

python -V

- Formatadores

pip install pylint autopep8

- Debug

pip install ipdb

import ipdb

ipdb.set_trace()

# Fix Bug Latin Encode Print Python

pip install ftfy

from ftfy import fix_encoding

text = fix_encoding(latin_text)

# Django
pip install django djangorestframework

- Create a project 

django-admin startproject kenzieFM .

- Create a app context in project django

./manage.py startapp name

- Make migrations (if get no changes, input the app in settings in instaled apps)

./manage.py makemigrations 

./manage.py migrate





